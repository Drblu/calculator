package mycompany.service;

import mycompany.core.ReversePolishNotationConverter;
import mycompany.core.ReversePolishNotationEvaluator;
import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BasicCalculatorServiceTest {
    @Mock
    private ReversePolishNotationConverter reversePolishNotationConverter;
    @Mock
    private ReversePolishNotationEvaluator reversePolishNotationEvaluator;

    CalculatorService calculatorService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        calculatorService = new BasicCalculatorService(reversePolishNotationConverter, reversePolishNotationEvaluator);
    }

    @Test
    public void should_convert_the_expression_to_polish_revert_notation_using_a_converter() {
        MathematicalExpression expression = new MathematicalExpression("An expression");

        calculatorService.evaluate(expression);

        verify(reversePolishNotationConverter).convert(expression);
    }

    @Test
    public void should_evaluate_the_converted_expression_polish_rever_notation_evaluator_and_return_the_result() {
        MathematicalExpression convertedExpression = new MathematicalExpression("The input expression");
        MathematicalExpression givenExpression = new MathematicalExpression("The input expression");
        when(reversePolishNotationConverter.convert(givenExpression)).thenReturn(convertedExpression);
        ExpressionResult expectedResult = new ExpressionResult(1);
        when(reversePolishNotationEvaluator.evaluate(convertedExpression)).thenReturn(expectedResult);

        ExpressionResult result = calculatorService.evaluate(givenExpression);

        verify(reversePolishNotationEvaluator).evaluate(convertedExpression);
        assertEquals(expectedResult, result);
    }
}