package mycompany.core;

import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReversePolishNotationEvaluatorImplTest {

    private ReversePolishNotationEvaluator evaluator;

    @Before
    public void setUp() {
        evaluator = new ReversePolishNotationEvaluatorImpl();
    }

    @Test
    public void should_evaluate_sum_correctly() {
        ExpressionResult expectedResult = new ExpressionResult(6);
        MathematicalExpression expression = new MathematicalExpression("1 2 3 + + ");

        ExpressionResult result = evaluator.evaluate(expression);

        assertEquals(expectedResult, result);
    }

    @Test
    public void should_evaluate_substraction_correctly() {
        ExpressionResult expectedResult = new ExpressionResult(1);
        MathematicalExpression expression = new MathematicalExpression("3 2 - ");

        ExpressionResult result = evaluator.evaluate(expression);

        assertEquals(expectedResult, result);
    }

    @Test
    public void should_evaluate_multiplication_correctly() {
        ExpressionResult expectedResult = new ExpressionResult(6);
        MathematicalExpression expression = new MathematicalExpression("3 2 * ");

        ExpressionResult result = evaluator.evaluate(expression);

        assertEquals(expectedResult, result);
    }

    @Test
    public void should_evaluate_division_correctly() {
        ExpressionResult expectedResult = new ExpressionResult(2);
        MathematicalExpression expression = new MathematicalExpression("4 2 / ");

        ExpressionResult result = evaluator.evaluate(expression);

        assertEquals(expectedResult, result);
    }

    @Test
    public void should_evaluate_a_mixed_operation_correctly() {
        ExpressionResult expectedResult = new ExpressionResult(13);
        MathematicalExpression expression = new MathematicalExpression("5 2 3 ^ + ");

        ExpressionResult result = evaluator.evaluate(expression);

        assertEquals(expectedResult, result);
    }

    @Test
    public void should_compute_an_expression_with_double_operators() {
        ExpressionResult expectedResult = new ExpressionResult(7.0);
        MathematicalExpression expression = new MathematicalExpression("1.2 2.2 3.6 + + ");

        ExpressionResult result = evaluator.evaluate(expression);

        assertEquals(expectedResult.getResult(), result.getResult(), 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_Exeption_when_divide_by_zero() {
        MathematicalExpression expression = new MathematicalExpression("4 0 / ");
        evaluator.evaluate(expression);
    }
}