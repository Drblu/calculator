package mycompany.core;

import mycompany.model.MathematicalExpression;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ShuntingYardConverterTest {

    private ReversePolishNotationConverter converter;

    @Before
    public void setUp() {
        converter = new ShuntingYardConverter();
    }

    @Test
    public void should_take_into_account_binary_operator_priority_with_left_assosiativity() {
        MathematicalExpression expectedConversion = new MathematicalExpression("5 7 2 / + ");
        MathematicalExpression inputExpression = new MathematicalExpression("5+7/2");

        MathematicalExpression convertedExpression = converter.convert(inputExpression);

        assertEquals(expectedConversion, convertedExpression);
    }

    @Test
    public void should_take_into_account_binary_operator_priority_with_right_assosiativity() {
        MathematicalExpression expectedConversion = new MathematicalExpression("2 2 3 ^ ^ ");
        MathematicalExpression inputExpression = new MathematicalExpression("2^2^3");

        MathematicalExpression convertedExpression = converter.convert(inputExpression);

        assertEquals(expectedConversion, convertedExpression);
    }

    @Test
    public void should_take_into_account_binary_operator_priority_with_mixed_associativity_operator() {
        MathematicalExpression expectedConversion = new MathematicalExpression("5 2 3 ^ + ");
        MathematicalExpression inputExpression = new MathematicalExpression("5+2^3");

        MathematicalExpression convertedExpression = converter.convert(inputExpression);

        assertEquals(expectedConversion, convertedExpression);
    }

    @Test
    public void should_take_into_account_parenthesis_in_expression() {
        MathematicalExpression expectedConversion = new MathematicalExpression("6 3 - 2 ^ 11 - ");
        MathematicalExpression inputExpression = new MathematicalExpression("(6-3)^2-11");

        MathematicalExpression convertedExpression = converter.convert(inputExpression);

        assertEquals(expectedConversion, convertedExpression);
    }

    @Test
    public void should_take_into_account_expression() {
        MathematicalExpression expectedConversion = new MathematicalExpression("3 2 * 1 5 / + ");
        MathematicalExpression inputExpression = new MathematicalExpression("3*2+1/5");

        MathematicalExpression convertedExpression = converter.convert(inputExpression);

        assertEquals(expectedConversion, convertedExpression);
    }
}