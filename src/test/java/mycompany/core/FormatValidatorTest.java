package mycompany.core;

import mycompany.model.MathematicalExpression;
import org.junit.Before;
import org.junit.Test;

public class FormatValidatorTest {

    private ExpressionValidator expressionValidator;

    @Before
    public void setUp() {
        expressionValidator = new FormatValidator();
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_exception_when_wrong_parenthesis() {
        MathematicalExpression mathematicalExpression = new MathematicalExpression("(1+2))");

        expressionValidator.validate(mathematicalExpression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_exception_when_argument_is_empty() {
        MathematicalExpression mathematicalExpression = new MathematicalExpression("");

        expressionValidator.validate(mathematicalExpression);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_an_exception_when_expression_contains_alpha_characters() {
        MathematicalExpression mathematicalExpression = new MathematicalExpression("a+1");

        expressionValidator.validate(mathematicalExpression);
    }
}