package mycompany.presenter;

import mycompany.core.ExpressionValidator;
import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;
import mycompany.service.CalculatorService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BasicCalculatorTest {

    @Mock
    private CalculatorService calculatorService;
    @Mock
    private ExpressionValidator expressionValidator;

    private CalculatorPresenter calculatorPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        calculatorPresenter = new BasicCalculator(calculatorService, expressionValidator);
    }

    @Test
    public void should_call_the_service_to_compute_the_expression() {
        MathematicalExpression mathematicalExpression = new MathematicalExpression("An expression");

        calculatorPresenter.compute(mathematicalExpression);

        verify(calculatorService).evaluate(mathematicalExpression);
    }

    @Test
    public void should_return_the_expected_result() {
        MathematicalExpression mathematicalExpression = new MathematicalExpression("An expression");
        ExpressionResult expectedResult = new ExpressionResult(1);
        when(calculatorService.evaluate(mathematicalExpression)).thenReturn(expectedResult);

        ExpressionResult computedResult = calculatorPresenter.compute(mathematicalExpression);

        assertEquals(expectedResult, computedResult);
    }

    @Test
    public void should_call_the_validator_in_order_to_verify_that_the_expression_is_well_formed() {
        MathematicalExpression mathematicalExpression = new MathematicalExpression("An expression");

        calculatorPresenter.compute(mathematicalExpression);

        verify(expressionValidator).validate(mathematicalExpression);
    }

}