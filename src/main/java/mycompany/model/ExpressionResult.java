package mycompany.model;

public class ExpressionResult {

    private double result;

    public ExpressionResult(double result) {
        this.result = result;
    }

    public double getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExpressionResult that = (ExpressionResult) o;

        return result == that.result;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(result);
        return (int) (temp ^ (temp >>> 32));
    }
}
