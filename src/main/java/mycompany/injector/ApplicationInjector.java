package mycompany.injector;

import com.google.inject.AbstractModule;
import mycompany.core.*;
import mycompany.presenter.BasicCalculator;
import mycompany.presenter.CalculatorPresenter;
import mycompany.service.BasicCalculatorService;
import mycompany.service.CalculatorService;

public class ApplicationInjector extends AbstractModule {

    @Override
    protected void configure() {
        bind(CalculatorPresenter.class).to(BasicCalculator.class);
        bind(CalculatorService.class).to(BasicCalculatorService.class);
        bind(ReversePolishNotationConverter.class).to(ShuntingYardConverter.class);
        bind(ReversePolishNotationEvaluator.class).to(ReversePolishNotationEvaluatorImpl.class);
        bind(ExpressionValidator.class).to(FormatValidator.class);
    }
}
