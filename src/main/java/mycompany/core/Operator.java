package mycompany.core;

public enum Operator {
    ADD(1, Associativity.LEFT),
    SUBTRACT(1, Associativity.LEFT),
    MULTIPLY(2, Associativity.LEFT),
    DIVIDE(2, Associativity.LEFT),
    POW(3, Associativity.RIGHT);

    private final int priority;
    private final Associativity direction;

    Operator(int priority, Associativity direction) {
        this.priority = priority;
        this.direction = direction;
    }

    public int getPriority() {
        return priority;
    }

    public Associativity getDirection() {
        return direction;
    }
}
