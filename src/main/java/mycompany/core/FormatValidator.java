package mycompany.core;

import mycompany.model.MathematicalExpression;

import java.util.Deque;
import java.util.LinkedList;

public class FormatValidator implements ExpressionValidator {

    @Override
    public void validate(MathematicalExpression mathematicalExpression) throws IllegalArgumentException{
        String expression = mathematicalExpression.getExpression();
        if (expression.isEmpty()) {
            throw new IllegalArgumentException("You cannot enter an empty expression");
        }

        verifyParenthesis(expression);
        verifyAlphacharacters(expression);
    }

    private void verifyParenthesis(String expression) {
        Deque<String> stack = new LinkedList<>();

        for (String element : expression.split("")) {
            if (element.equals("(")) {
                stack.push(element);
            }

            if (element.equals(")")) {
                if (stack.isEmpty()) {
                    throw new IllegalArgumentException("Wrong parenthesis in expression");
                }

                String lastElement = stack.peek();
                if (element.equals(")") && lastElement.equals("(")) {
                    stack.pop();
                } else {
                    throw new IllegalArgumentException("Wrong parenthesis in expression");
                }
            }
        }
        if (!stack.isEmpty()) {
            throw new IllegalArgumentException("Wrong parenthesis in expression");
        }
    }

    private void verifyAlphacharacters(String expression) {
        if (expression.matches(".*[A-Za-z].*")) {
            throw new IllegalArgumentException("The expression cannot contain any characters");
        }
    }
}
