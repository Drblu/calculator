package mycompany.core;

import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;

public interface ReversePolishNotationEvaluator {

    ExpressionResult evaluate(MathematicalExpression mathematicalExpression) throws IllegalArgumentException;
}
