package mycompany.core;

import mycompany.model.MathematicalExpression;

public interface ExpressionValidator {

    void validate(MathematicalExpression mathematicalExpression) throws IllegalArgumentException;
}
