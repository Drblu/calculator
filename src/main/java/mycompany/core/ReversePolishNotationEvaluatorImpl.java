package mycompany.core;

import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Deque;
import java.util.LinkedList;

public class ReversePolishNotationEvaluatorImpl implements ReversePolishNotationEvaluator {

    public static final String ZERO_ARGUMENT = "The second argument of the expression cannot be null";

    @Override
    public ExpressionResult evaluate(MathematicalExpression mathematicalExpression) throws IllegalArgumentException{
        double temp;
        Deque<String> stack = new LinkedList<>();
        double firstOperand;
        double secondOperand;

        for (String element : mathematicalExpression.getExpression().split(" ")) {
            if (NumberUtils.isCreatable(element)) {
                stack.push(element);
            } else {
                firstOperand = Double.parseDouble(stack.pop());
                secondOperand = Double.parseDouble(stack.pop());
                switch (element) {
                    case "+":
                        temp = secondOperand + firstOperand ;
                        stack.push(Double.toString(temp));
                        break;
                    case "-":
                        temp = secondOperand - firstOperand;
                        stack.push(Double.toString(temp));
                        break;
                    case "*":
                        temp = secondOperand * firstOperand;
                        stack.push(Double.toString(temp));
                        break;
                    case "/":
                        if(firstOperand == 0)
                            throw new IllegalArgumentException(ZERO_ARGUMENT);
                        temp = secondOperand / firstOperand;
                        stack.push(Double.toString(temp));
                        break;
                    case "^":
                        double temporary = Math.pow(secondOperand, firstOperand);
                        stack.push(Double.toString((int)temporary));
                        break;
                    default:
                        break;
                }
            }
        }
        return new ExpressionResult(Double.parseDouble(stack.pop()));
    }
}
