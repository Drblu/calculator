package mycompany.core;

import mycompany.model.MathematicalExpression;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ShuntingYardConverter implements ReversePolishNotationConverter {

    @Override
    public MathematicalExpression convert(MathematicalExpression mathematicalExpression) {
        StringBuilder convertedResult = new StringBuilder();
        Deque<String> stack = new LinkedList<>();

        // Define +-*/^ and ( ) as delimiters but still take them as token
        String delimiters = "((?<=\\+)|(?=\\+))|((?<=-)|(?=-))|((?<=\\*)|" +
                "(?=\\*))|((?<=/)|(?=/))|((?<=\\^)|(?=\\^))|((?<=\\x28)|(?=\\x28))|((?<=\\x29)|(?=\\x29))";

        for (String token : mathematicalExpression.getExpression().split(delimiters)) {
            if (operators.containsKey(token)) {
                Operator actualOperator = operators.get(token);
                while (!stack.isEmpty() && operators.containsKey(stack.peek()) && ((actualOperator.getDirection() == Associativity.LEFT
                        && actualOperator.getPriority() <= operators.get(stack.peek()).getPriority())
                        || (actualOperator.getDirection() == Associativity.RIGHT &&
                        actualOperator.getPriority() < operators.get(stack.peek()).getPriority()))) {
                    convertedResult.append(stack.pop()).append(' ');
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.peek().equals("(")) {
                    convertedResult.append(stack.pop()).append(' ');
                }
                stack.pop();
            } else {
                convertedResult.append(token).append(' ');
            }
        }
        while (!stack.isEmpty()) {
            convertedResult.append(stack.pop()).append(' ');
        }

        return new MathematicalExpression(convertedResult.toString());
    }

    private static Map<String, Operator> operators = new HashMap<String, Operator>() {{
        put("+", Operator.ADD);
        put("-", Operator.SUBTRACT);
        put("*", Operator.MULTIPLY);
        put("/", Operator.DIVIDE);
        put("^", Operator.POW);
    }};
}
