package mycompany.core;

import mycompany.model.MathematicalExpression;

public interface ReversePolishNotationConverter {

    MathematicalExpression convert(MathematicalExpression mathematicalExpression);
}
