package mycompany.service;

import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;

public interface CalculatorService {
    ExpressionResult evaluate(MathematicalExpression mathematicalExpression) throws IllegalArgumentException;
}
