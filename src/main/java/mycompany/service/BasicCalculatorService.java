package mycompany.service;

import com.google.inject.Inject;
import mycompany.core.ReversePolishNotationConverter;
import mycompany.core.ReversePolishNotationEvaluator;
import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;

public class BasicCalculatorService implements CalculatorService{

    private final ReversePolishNotationConverter reversePolishNotationConverter;
    private final ReversePolishNotationEvaluator reversePolishNotationEvaluator;

    @Inject
    public BasicCalculatorService(ReversePolishNotationConverter reversePolishNotationConverter, ReversePolishNotationEvaluator reversePolishNotationEvaluator) {
        this.reversePolishNotationConverter = reversePolishNotationConverter;
        this.reversePolishNotationEvaluator = reversePolishNotationEvaluator;
    }

    @Override
    public ExpressionResult evaluate(MathematicalExpression mathematicalExpression) throws IllegalArgumentException{
        MathematicalExpression convertedExpression = reversePolishNotationConverter.convert(mathematicalExpression);
        return reversePolishNotationEvaluator.evaluate(convertedExpression);
    }
}
