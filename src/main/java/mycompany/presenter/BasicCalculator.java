package mycompany.presenter;

import com.google.inject.Inject;
import mycompany.core.ExpressionValidator;
import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;
import mycompany.service.CalculatorService;

public class BasicCalculator implements CalculatorPresenter {

    private final CalculatorService calculatorService;
    private final ExpressionValidator expressionValidator;

    @Inject
    public BasicCalculator(CalculatorService calculatorService, ExpressionValidator expressionValidator) {
        this.calculatorService = calculatorService;
        this.expressionValidator = expressionValidator;
    }

    @Override
    public ExpressionResult compute(MathematicalExpression mathematicalExpression) throws IllegalArgumentException{
        expressionValidator.validate(mathematicalExpression);
        return calculatorService.evaluate(mathematicalExpression);
    }
}
