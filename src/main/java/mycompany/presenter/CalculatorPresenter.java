package mycompany.presenter;

import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;

public interface CalculatorPresenter {

    ExpressionResult compute(MathematicalExpression mathematicalExpression) throws IllegalArgumentException;
}
