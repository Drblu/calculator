package mycompany;

import com.google.inject.Guice;
import com.google.inject.Injector;
import mycompany.injector.ApplicationInjector;
import mycompany.model.ExpressionResult;
import mycompany.model.MathematicalExpression;
import mycompany.presenter.CalculatorPresenter;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        MathematicalExpression mathematicalExpression;

        Injector injector = Guice.createInjector(new ApplicationInjector());
        CalculatorPresenter calculatorPresenter = injector.getInstance(CalculatorPresenter.class);

        while (true) {
            System.out.println("Enter an expression to evaluate:\n");
            Scanner scanner = new Scanner(System.in);
            mathematicalExpression = new MathematicalExpression(scanner.nextLine().replaceAll("\\s",""));

            try{
                ExpressionResult computedResult = calculatorPresenter.compute(mathematicalExpression);
                System.out.println("= " + computedResult.getResult());
            } catch (IllegalArgumentException exception){
                System.out.println(exception.getMessage());
            }

        }
    }
}
